package Modelo;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author Joaquin
 */
public class BDConnect {

    private static Connection conn = null;

    /**
     * Metodo para crear la conexion a la base de datos tiendas
     *
     * @return
     */
    public static Connection getConnection() {
        try {
            if (conn == null) {
                Runtime.getRuntime().addShutdownHook(new MiShDwHook());
                String driver = "com.mysql.jdbc.Driver";
                String url = "jdbc:mysql://localhost/tiendas";
                String usuario = "root";
                String password = "";
                Class.forName(driver);
                conn = DriverManager.getConnection(url, usuario, password);
            }

            return conn;
        } catch (SQLException | ClassNotFoundException ex) {
            System.out.println("no se hace la conexion");
            ex.printStackTrace();
            return null;
        }

    }

    /**
     * Metodo que genera una conexion a mysql sin especificar la base de datos
     * para asi poder crearla desde java
     *
     * @return
     */
    public static Connection getConnectionCreate() {
        try {
            if (conn == null) {
                Runtime.getRuntime().addShutdownHook(new MiShDwHook());
                String driver = "com.mysql.jdbc.Driver";
                String url = "jdbc:mysql://localhost";
                String usuario = "root";
                String password = "";
                Class.forName(driver);
                conn = DriverManager.getConnection(url, usuario, password);
            }

            return conn;
        } catch (SQLException | ClassNotFoundException ex) {
            return null;
        }

    }

    static class MiShDwHook extends Thread {

        @Override
        public void run() {
            try {
                Connection conn = BDConnect.getConnection();
                conn.close();
            } catch (SQLException ex) {
                System.out.println("No se ha podido cerrar la conexion.");
            }
        }
    }
}
