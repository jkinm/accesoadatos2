/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author Joaquin
 */
public class BDCreate {
//

    private Connection con = BDConnect.getConnectionCreate();
    private Connection con1 = BDConnect.getConnection();

    /**
     * Metodo para crear la base de datos
     *
     * @throws SQLException
     */
    public void createDB() throws SQLException {
        try {
            Statement stm = con.createStatement();
            stm.executeUpdate("CREATE DATABASE IF NOT EXISTS tiendas");
            System.out.println("Se ha creado la DB");
            stm.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
//    private Connection con1 = BDConnect.getConnection();

    /**
     * Metodo para crear las tablas de las base de datos y la clave foranea
     *
     * @throws SQLException
     */
    public void createTables() throws SQLException {
        try {

            con1.setAutoCommit(false);
            Statement stm = con1.createStatement();
            stm.executeUpdate("CREATE TABLE IF NOT EXISTS tiendas.articulo ("
                    + "  id int(11) NOT NULL AUTO_INCREMENT,"
                    + "  idtienda int(11) NOT NULL,"
                    + "  nombre varchar(254) NOT NULL,"
                    + "  descripcion varchar(1024) NOT NULL,"
                    + "  precio double NOT NULL,"
                    + "  PRIMARY KEY (id),"
                    + "  KEY idtienda (idtienda)"
                    + ") ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1");
            con1.commit();
            stm.close();
        } catch (Exception e) {
            con1.rollback();
            con1.setAutoCommit(true);
        }
        try {
            con1.setAutoCommit(false);
            Statement stm = con1.createStatement();
            stm.executeUpdate("CREATE TABLE IF NOT EXISTS tiendas.tienda ("
                    + "  id int(11) NOT NULL AUTO_INCREMENT,"
                    + "  nif varchar(9) NOT NULL,"
                    + "  nombre varchar(256) NOT NULL,"
                    + "  telefono int(9) NOT NULL,"
                    + "  direccion varchar(256) NOT NULL,"
                    + "  PRIMARY KEY (id)"
                    + ") ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1");
            con1.commit();
            stm.close();
        } catch (Exception e) {
            con1.rollback();
            con1.setAutoCommit(true);
        }
        try {
            con1.setAutoCommit(false);

            Statement stm = con1.createStatement();
            stm.executeUpdate("ALTER TABLE tiendas.articulo"
                    + "  ADD CONSTRAINT articulo_ibfk_1 FOREIGN KEY (idtienda) REFERENCES tiendas.tienda (id) ON DELETE CASCADE ON UPDATE CASCADE");
            con1.commit();
            System.out.println("Se han creado las tablas");
            stm.close();
        } catch (Exception e) {
            con1.rollback();
            con1.setAutoCommit(true);
            e.printStackTrace();
        }

    }

}
