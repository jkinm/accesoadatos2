/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.io.Serializable;

/**
 *
 * @author Joaquin
 */
public class Articulo implements Serializable {

    private int id;
    private int tiendaId;
    private String nombre;
    private String descripcion;
    private double precio;

    /**
     *
     * @param id
     * @param tiendaId
     * @param nombre
     * @param descripcion
     * @param precio
     */
    public Articulo(int id, int tiendaId, String nombre, String descripcion, double precio) {
        this.id = id;
        this.tiendaId = tiendaId;
        this.nombre = nombre;
        this.descripcion = descripcion;
        this.precio = precio;
    }

    /**
     *
     * @return
     */
    public int getTiendaId() {
        return tiendaId;
    }

    /**
     *
     * @param tiendaId
     */
    public void setTiendaId(int tiendaId) {
        this.tiendaId = tiendaId;
    }

    /**
     *
     * @return
     */
    public int getId() {
        return id;
    }

    /**
     *
     * @param id
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     *
     * @return
     */
    public String getNombre() {
        return nombre;
    }

    /**
     *
     * @param nombre
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     *
     * @return
     */
    public String getDescripcion() {
        return descripcion;
    }

    /**
     *
     * @param descripcion
     */
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    /**
     *
     * @return
     */
    public double getPrecio() {
        return precio;
    }

    /**
     *
     * @param precio
     */
    public void setPrecio(double precio) {
        this.precio = precio;
    }

}
