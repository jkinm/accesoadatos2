/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.sql.SQLException;

/**
 *
 * @author Joaquin
 */
public class prueba {

    /**
     * Metodo que crea datos de prueba en la base de datos
     *
     * @throws SQLException
     */
    public static void crearDatosPrueba() throws SQLException {
        ArticuloDAO daoArt = new ArticuloDAO();
        TiendaDAO daoTienda = new TiendaDAO();

        daoArt.getListaArticulos().removeAll(daoArt.getListaArticulos());
        daoTienda.getListaTiendas().removeAll(daoTienda.getListaTiendas());

        for (int i = 1; i < 10; i++) {
            String nif = "NIF:" + i;
            int id = i;
            String nombre = "Nombre:" + i;
            int telefono = (i + 3) * 25663;
            String direccion = "Direccion:" + i;
            daoTienda.anadirTiendaBD(new Tienda(id, nif, nombre, telefono, direccion));
            for (int j = 1; j < 5; j++) {
                int idArt = j;
                String nombreArt = "NombreArt:" + j;
                String descripcionArt = "DescripcionArt:" + j;
                double precioArt = Math.random() * 100;

                daoArt.anadirArticuloBD(new Articulo(idArt, id, nombreArt, descripcionArt, precioArt));
                //System.out.println(idArt + "  " + id + "  " + nombreArt + "  " + descripcionArt + "  " + precioArt);
            }

        }

    }
}
