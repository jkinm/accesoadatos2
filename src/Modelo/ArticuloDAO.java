/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.util.ArrayList;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author Joaquin
 */
public class ArticuloDAO {

    private static ArrayList<Articulo> listaArticulos = new ArrayList<Articulo>();
    private Connection con = BDConnect.getConnection();

    /**
     * Metodo para añadir un elemento al arraylist
     *
     * @param a
     */
    public void anadirArticulo(Articulo a) {
        listaArticulos.add(a);
    }

    /**
     *
     * @return
     */
    public ArrayList<Articulo> getListaArticulos() {
        return listaArticulos;
    }

    /**
     *
     * @param listaArticulos
     */
    public void setListaArticulos(ArrayList<Articulo> listaArticulos) {
        ArticuloDAO.listaArticulos = listaArticulos;
    }

    /**
     * Metodo de prueba que muestra el arraylist por pantalla
     */
    public void muestraArrayPorPantalla() {
        for (int i = 0; i < listaArticulos.size(); i++) {
            String cad = listaArticulos.get(i).getId() + "," + listaArticulos.get(i).getTiendaId() + "," + listaArticulos.get(i).getNombre().toString() + ","
                    + listaArticulos.get(i).getDescripcion().toString() + "," + listaArticulos.get(i).getPrecio();
            System.out.println(cad);
        }
    }

    /**
     * Metodo para cargar los elementos de la BD en el arraylist
     *
     * @throws SQLException
     */
    public void cargaBD() throws SQLException {
        listaArticulos.removeAll(listaArticulos);
        Statement stm = con.createStatement();
        ResultSet rs = stm.executeQuery("select * from tiendas.articulo");
        String[] datos = new String[5];
        while (rs.next()) {
            datos[0] = rs.getString("id");
            datos[1] = rs.getString("idtienda");
            datos[2] = rs.getString("nombre");
            datos[3] = rs.getString("descripcion");
            datos[4] = rs.getString("precio");
            Articulo a = new Articulo(Integer.parseInt(datos[0]), Integer.parseInt(datos[1]), datos[2], datos[3], Double.parseDouble(datos[4]));
            listaArticulos.add(a);
        }
        stm.close();
    }

    /**
     * Metodo para añadir un articulo a la BD
     *
     * @param a
     * @throws SQLException
     */
    public void anadirArticuloBD(Articulo a) throws SQLException {
        try {

            Statement stm = con.createStatement();
            con.setAutoCommit(false);
            String consulta = "Insert into tiendas.articulo "
                    + "(idtienda,nombre,descripcion,precio)"
                    + " values (" + a.getTiendaId() + ",'" + a.getNombre() + "','" + a.getDescripcion() + "','" + a.getPrecio() + "')";
            System.out.println(consulta);
            stm.executeUpdate(consulta);
            con.commit();
            stm.close();
        } catch (Exception e) {
            con.rollback();
            con.setAutoCommit(true);
        }

    }

    /**
     * Metodo para borrar un articulo de la BD
     *
     * @param id
     * @param idTienda
     * @throws SQLException
     */
    public void borrarArticuloBD(int id, int idTienda) throws SQLException {
        try {

            con.setAutoCommit(false);
            Statement stm = con.createStatement();
            String consulta = "Delete from tiendas.articulo where id =" + id + " and idtienda =" + idTienda;
            System.out.println(consulta);
            stm.executeUpdate(consulta);
            con.commit();
            stm.close();
        } catch (Exception e) {
            con.rollback();
            con.setAutoCommit(true);
        }
    }

    /**
     * Metodo para actualizar un articulo de la BD
     *
     * @param a
     * @throws SQLException
     */
    public void actualizarArticuloBD(Articulo a) throws SQLException {
        try {

            con.setAutoCommit(false);
            Statement stm = con.createStatement();
            String consulta = "Update tiendas.articulo set "
                    + "idtienda='" + a.getTiendaId() + "', nombre='" + a.getNombre()
                    + "', descripcion='" + a.getDescripcion() + "', precio='" + a.getPrecio() + "'where id =" + a.getId() + " and idtienda=" + a.getTiendaId();
            stm.executeUpdate(consulta);
            con.commit();

            stm.close();
        } catch (Exception e) {
            con.rollback();
            con.setAutoCommit(true);
        }
    }

}
