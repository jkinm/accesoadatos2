/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author Joaquin
 */
public class TiendaDAO {

    private static ArrayList<Tienda> listaTiendas = new ArrayList<Tienda>();
    private Connection con = BDConnect.getConnection();

    /**
     *Metodo para añadir una tienda al arraylist
     * @param t
     */
    public void anadirTienda(Tienda t) {
        listaTiendas.add(t);
    }

    /**
     *
     * @return
     */
    public ArrayList<Tienda> getListaTiendas() {
        return listaTiendas;
    }

    /**
     *
     * @param listaTiendas
     */
    public void setListaTiendas(ArrayList<Tienda> listaTiendas) {
        TiendaDAO.listaTiendas = listaTiendas;
    }

    /**
     *Metodo de prueba que muestra el arraylist por pantalla
     */
    public void muestraArrayPorPantalla() {
        for (int i = 0; i < listaTiendas.size(); i++) {
            String cad = listaTiendas.get(i).getId() + "," + listaTiendas.get(i).getNif() + "," + listaTiendas.get(i).getNombre()
                    + "," + listaTiendas.get(i).getTelefono() + "," + listaTiendas.get(i).getDireccion();
            System.out.println(cad);
        }
    }

    

    /**
     *Metodo que carga los elementos de la base de datos en el array list
     * @throws SQLException
     */

    public void cargaBD() throws SQLException {
        listaTiendas.removeAll(listaTiendas);
        Statement stm = con.createStatement();
        ResultSet rs = stm.executeQuery("select * from tiendas.tienda");
        String[] datos = new String[5];
        while (rs.next()) {
            datos[0] = rs.getString("id");
            datos[1] = rs.getString("nif");
            datos[2] = rs.getString("nombre");
            datos[3] = rs.getString("telefono");
            datos[4] = rs.getString("direccion");
            Tienda t = new Tienda(Integer.parseInt(datos[0]), datos[1], datos[2], Integer.parseInt(datos[3]), datos[4]);
            listaTiendas.add(t);
        }
        stm.close();
    }

    /**
     *Metodo que añade una tienda a la base de datos
     * @param t
     * @throws SQLException
     */
    public void anadirTiendaBD(Tienda t) throws SQLException {
        try {
            con.setAutoCommit(false);
            Statement stm = con.createStatement();
            String consulta = "Insert into tiendas.tienda "
                    + "(nif,nombre,telefono,direccion)"
                    + "values('" + t.getNif() + "','" + t.getNombre() + "','" + t.getTelefono() + "','" + t.getDireccion() + "')";

            stm.executeUpdate(consulta);
            con.commit();
            stm.close();

        } catch (Exception e) {
            con.rollback();
            con.setAutoCommit(true);
        }
    }

    /**
     *Metodo que borra una tienda de la base de datos
     * @param id
     * @throws SQLException
     */
    public void borrarTiendaBD(int id) throws SQLException {

        try {

            con.setAutoCommit(false);
            Statement stm = con.createStatement();
            String consulta = "Delete from tiendas.tienda where id =" + id;
            stm.executeUpdate(consulta);
            con.commit();
            stm.close();
        } catch (Exception e) {
            con.rollback();
            con.setAutoCommit(true);
        }
    }

    /**
     *Metodo que actualiza una tienda de la base de datos
     * @param t
     * @throws SQLException
     */
    public void actualizarTiendaBD(Tienda t) throws SQLException {
        try {
            con.setAutoCommit(false);
            Statement stm = con.createStatement();
            String consulta = "Update tiendas.tienda set "
                    + "nif='" + t.getNif() + "', nombre='" + t.getNombre()
                    + "', telefono='" + t.getTelefono() + "', direccion='" + t.getDireccion() + "' where id =" + t.getId();
            stm.executeUpdate(consulta);
            con.commit();
            stm.close();

        } catch (Exception e) {
            con.rollback();
            con.setAutoCommit(true);
        }

    }
}
